# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Test linkahead.connection.utils."""
# pylint: disable=missing-docstring
from __future__ import unicode_literals, print_function
from pytest import raises

from linkahead.exceptions import ConfigurationError, LoginFailedError
from linkahead.connection.utils import parse_auth_token, auth_token_to_cookie
from linkahead.connection.connection import (
    configure_connection, CaosDBServerConnection,
    _DefaultCaosDBServerConnection)
from linkahead.connection.mockup import (MockUpServerConnection, MockUpResponse,
                                         _request_log_message)
from linkahead.configuration import get_config, _reset_config
from linkahead.connection.authentication.interface import CredentialsAuthenticator
from linkahead import execute_query


def eq(a, b):
    assert a == b


def there(a):
    assert a is not None


def tru(a):
    assert a


def setup_module():
    _reset_config()


def test_parse_auth_token():
    assert parse_auth_token(
        "SessionToken=%5Bblablabla%5D; expires=bla; ...") == "[blablabla]"


def test_auth_token_to_cookie():
    assert auth_token_to_cookie(
        "[blablabla]") == "SessionToken=%5Bblablabla%5D;"
