
# -*- encoding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
# Copyright (C) 2024 Joscha Schmiedt <joscha@schmiedt.dev>
# Copyright (C) 2020-2024 IndiScale GmbH <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Tests for the Container class."""
import pytest

import linkahead as db


def test_get_property_values():
    rt_house = db.RecordType("House")
    rt_window = db.RecordType("Window")
    rt_owner = db.RecordType("Owner")
    p_height = db.Property("Height", datatype=db.DOUBLE)

    window = db.Record().add_parent(rt_window)
    window.id = 1001
    window.add_property(p_height, 20.5, unit="m")

    owner = db.Record("The Queen").add_parent(rt_owner)

    house = db.Record("Buckingham Palace")
    house.add_parent(rt_house)
    house.add_property(rt_owner, owner)
    house.add_property(rt_window, window)
    house.add_property(p_height, 40.2, unit="ft")

    container = db.Container()
    container.extend([
        house,
        owner
    ])

    assert getattr(house.get_property(p_height), "unit") == "ft"
    assert getattr(window.get_property(p_height), "unit") == "m"

    table = container.get_property_values("naME",
                                          "height",
                                          ("height", "unit"),
                                          "window",
                                          ("window", "non-existing"),
                                          ("window", "non-existing", "unit"),
                                          ("window", "unit"),
                                          ("window", "heiGHT"),
                                          ("window", "heiGHT", "value"),
                                          ("window", "heiGHT", "unit"),
                                          "owner",
                                          )
    assert len(table) == 2
    house_row = table[0]
    assert house_row == (house.name, 40.2, "ft", window.id, None, None, None, 20.5, 20.5, "m",
                         owner.name)

    owner_row = table[1]
    assert owner_row == (owner.name, None, None, None, None, None, None, None, None, None, None)

    assert container.get_property_values("non-existing") == [(None,), (None,)]
    assert container.get_property_values("name") == [(house.name,),
                                                     (owner.name,)]


def test_container_dependencies_for_deletion():
    not_included_rt = 1000
    rt = db.RecordType("Just a RecordType")
    rt.id = 1001
    rt_record_with_parent = db.RecordType("Records with parent")
    rt_record_with_parent.id = 1005
    property_which_is_not_a_record = db.Property(
        "Normal Property", datatype=db.DOUBLE, value=1006)
    property_which_is_not_a_record.id = 1006
    property_which_shall_be_deleted = db.Property(
        "Normal Property 2", datatype=db.DOUBLE, value=1006)
    property_which_shall_be_deleted .id = 1007

    record_without_dependencies = db.Record().add_parent(not_included_rt)
    record_without_dependencies.id = 2003

    record_referenced = db.Record().add_parent(not_included_rt)
    record_referenced.id = 2002
    record_with_dependencies = db.Record().add_parent(not_included_rt)
    record_with_dependencies.id = 2004
    record_with_dependencies.add_property(not_included_rt,
                                          record_referenced,
                                          datatype="not_included_rt")

    record_with_parent = db.Record().add_parent(rt_record_with_parent)
    record_with_parent.id = 2005

    record_with_property_which_is_not_a_record = db.Record(
    ).add_parent(not_included_rt)
    record_with_property_which_is_not_a_record.id = 2006
    record_with_property_which_is_not_a_record.add_property(
        property_which_is_not_a_record)
    record_with_property_which_is_not_a_record.add_property(
        property_which_shall_be_deleted)

    container = db.Container()
    container.extend([
        rt,
        rt_record_with_parent,  # 1005, dependency
        record_without_dependencies,
        property_which_shall_be_deleted,  # 1007, dependency
        record_referenced,  # 2002, dependency
        record_with_dependencies,
        record_with_parent,
        record_with_property_which_is_not_a_record
    ])
    assert (db.Container._find_dependencies_in_container(container)
            == {2002, 1005, 1007})


def test_container_dependencies_for_deletion_with_lists():
    not_included_rt = 1000

    record_referenced = db.Record().add_parent(not_included_rt)
    record_referenced.id = 2001

    record_with_list = db.Record().add_parent(not_included_rt)
    record_with_list.id = 2002
    record_with_list.add_property(not_included_rt, datatype=db.LIST(
        not_included_rt), value=[record_referenced, 2003, 2004, 2005, 2006])

    container = db.Container()
    container.extend([record_with_list, record_referenced])

    assert db.Container._find_dependencies_in_container(container) == {2001}


def test_container_deletion_with_references():
    """Test if dependencies are checked correctly.
    """

    RT1 = db.RecordType(name="RT1")
    RT2 = db.RecordType(name="RT2").add_property(name="prop2", datatype=RT1)
    RT3 = db.RecordType(name="RT3").add_property(name="prop3", datatype="LIST<RT1>")
    prop4a = db.Property(name="prop4a", datatype=RT1)
    prop4b = db.Property(name="prop4b", datatype="RT1")
    prop5 = db.Property(name="prop5", datatype="LIST<RT1>")
    cont12 = db.Container().extend([RT1, RT2])
    cont13 = db.Container().extend([RT1, RT3])
    cont14a = db.Container().extend([RT1, prop4a])
    cont14b = db.Container().extend([RT1, prop4b])
    cont15 = db.Container().extend([RT1, prop5])
    cont12.to_xml()
    cont13.to_xml()
    cont14a.to_xml()
    cont14b.to_xml()
    cont15.to_xml()

    deps12 = db.Container._find_dependencies_in_container(cont12)
    deps13 = db.Container._find_dependencies_in_container(cont13)
    deps14a = db.Container._find_dependencies_in_container(cont14a)
    deps14b = db.Container._find_dependencies_in_container(cont14b)
    deps15 = db.Container._find_dependencies_in_container(cont15)

    assert len(deps12) == 1 and deps12.pop() == -1
    assert len(deps13) == 1 and deps13.pop() == -1
    assert len(deps14a) == 1 and deps14a.pop() == -1
    assert len(deps14b) == 1 and deps14b.pop() == -1
    assert len(deps15) == 1 and deps15.pop() == -1


def test_container_slicing():
    cont = db.Container()
    cont.extend([db.Record(name=f"TestRec{ii+1}") for ii in range(5)])
    assert isinstance(cont, db.common.models.Container)
    container_slice = cont[:2]
    assert isinstance(container_slice, db.common.models.Container), \
        f"Container slice should be Container, was {type(container_slice)}"
    for element in container_slice:
        assert isinstance(element, db.Record), \
            f"element in slice was not Record, but {type(element)}"
    assert len(container_slice) == 2
    assert cont[-1].name == "TestRec5"

    with pytest.raises(TypeError):
        cont["stringkey"]

    with pytest.raises(TypeError):
        cont[[0, 2, 3]]


def test_container_filter():
    # this is a very rudimentary test since filter_by_identity is based on
    # _filter_entity_list_by_identity which is tested
    # separately
    cont = db.Container()
    cont.extend([db.Record(name=f"TestRec{ii+1}") for ii in range(5)])

    recs = cont.filter_by_identity(name="TestRec2")
    assert len(recs) == 1
    recs[0].name == "TestRec2"
