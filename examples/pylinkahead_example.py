#!/usr/bin/env python3
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2024 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""A small example to get started with linkahead-pylib.

Make sure that a `pylinkahead.ini` is readable at one of the expected locations.
"""

import random

import caosdb as db


def reconfigure_connection():
    """Change the current connection configuration."""
    conf = db.configuration.get_config()
    conf.set("Connection", "url", "https://demo.indiscale.com")
    db.configure_connection()


def main():
    """Shows a few examples how to use the LinkAhead library."""
    conf = dict(db.configuration.get_config().items("Connection"))
    print("##### Config:\n{}\n".format(conf))

    if conf["cacert"] == "/path/to/caosdb.ca.pem":
        print("Very likely, the path to the TLS certificate is not correct, "
              "please fix it.")

    # Query the server, the result is a Container
    result = db.Query("FIND Record").execute()
    print("##### First query result:\n{}\n".format(result[0]))

    # Retrieve a random Record
    rec_id = random.choice([rec.id for rec in result])
    rec = db.Record(id=rec_id).retrieve()
    print("##### Randomly retrieved Record:\n{}\n".format(rec))


if __name__ == "__main__":
    main()
