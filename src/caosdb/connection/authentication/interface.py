
from linkahead.connection.authentication.interface import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.connection.authentication.interface`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
