#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
import os
base_root = "src/linkahead/"

initcontent = """
from {module} import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import {module}`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
"""

for root, dirs, files in os.walk(base_root, topdown=False):
    if root.endswith("__pycache__"):
        continue
    cdir = os.path.join("src/caosdb", root[len(base_root):])
    os.makedirs(cdir, exist_ok=True)
    for fi in files:
        if not fi.endswith(".py"):
            continue
        path = os.path.join(cdir, fi)
        with open(path, 'w') as cur:
            if fi == "__init__.py":
                cur.write(initcontent.format(module=".".join(
                    os.path.join(root, fi[:-3]).split('/')[1:-1])))
            else:
                cur.write(initcontent.format(module=".".join(
                    os.path.join(root, fi[:-3]).split('/')[1:])))
