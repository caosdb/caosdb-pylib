========================================
Managing data from numerical simulations
========================================

This code example

1. sets up the data model
2. runs simulations
3. stores the simulation parameters and results into LinkAhead
4. retrieves the parameters for interesting results.

:download:`Download code<simulation.py>`

.. literalinclude:: simulation.py
