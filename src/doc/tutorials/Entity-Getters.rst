
Entity Getters
==============

There is a very frequent situation when working with PyLinkAhead: You need to get a specific Entity
from the remote server. For example, you need the Property Entity in order to make an update. Sure,
you can do a ``db.Entity().retrieve()`` or submit a query, but there is an even faster way which
also helps preventing errors:

- ``get_entity_by_name``
- ``get_entity_by_id``
- ``get_entity_by_path``

You can call these functions with a single argument (name/id/path). Since these functions are
frequently used with the same arguments over and over again, you might want to look at the
:doc:`caching options<caching>`.
