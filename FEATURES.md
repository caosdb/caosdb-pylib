# Experimental Features

- High Level API in the module `high_level_api` is experimental and might be removed in future. It is for playing around with a possible future implementation of the Python client. See `src/doc/future_caosdb.md`


# Features
TODO: This is currently an incomplete list.
- `to_graphics` defined in `caosdb.utils.plantuml` can be used to create an UML diagram of a data model
