
Setting permissions for a curator role
======================================

The following example shows how to create and set permissions for a ``curator``
role that is allowed to insert, update, or delete any entity apart from a set of
RecordTypes and properties that define a "core data model" which can only be
altered with administration permissions.

In the following, you'll learn how to

1. create the ``curator`` role.
2. configure the ``global_entity_permissions.xml`` s.th. the ``curator`` role is
   allowed to insert, update, or delete any entity by default.
3. use a Python script to override the above configuration for the entities in
   the externally defined core data model.

Prerequisites
-------------

This example needs some preparations regarding your LinkAhead setup that have to
(or, for the sake of simplicity, should) be done outside the actual Python
example script.

The curator role
~~~~~~~~~~~~~~~~

First, a ``curator`` role is created with a meaningful description. We'll use
``linkahead_admin.py`` for this which leads to the following command:

.. code:: console

   $ linkahead_admin.py create_role "curator" "A user who is permitted to create new Records, Properties, and RecordTypes but who is not allowed to change the core data model."

To actually see how this role's permissions change, we also need a user with
this role. Assume you already have created and activated (see
:doc:`Administration <../administration>`) a ``test_curator`` user, then
``linkahead_admin.py`` is used again to assign it the correct role:

.. code:: console

   $ linkahead_admin.py add_user_roles test_curator curator

.. note::

   The ``test_curator`` user shouldn't have administration privileges, otherwise
   the below changes won't have any effect.

The core data model and linkahead-advanced-user-tools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In principle, the following script works with any data model defined in a json
or yaml file (just adapt lines 39-42 accordingly). In this example, we'll use the
`metadata schema <https://github.com/leibniz-zmt/zmt-metadata-schema>`_ that was
developed by J. Schmidt at the `Leibniz Centre for Tropical Marine Research
<https://www.leibniz-zmt.de/en/>`_.

Clone the schemata into the same directory containing the below script via

.. code:: console

   $ git clone https://github.com/leibniz-zmt/zmt-metadata-schema.git

Furthermore, we'll need the `LinkAhead Advanced User Tools
<https://gitlab.com/linkahead/linkahead-advanced-user-tools>`_ for loading the
metadata schemata from the json files, so install them via

.. code:: console

   $ pip install caosadvancedtools

The global entity permissions file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Users with the ``curator`` role should be able to have any permission for all
entities by default. The exceptions for the core data model entities will be set
with the script below. These default settings are best done via the
``global_entities_permissions.xml`` config file (see the `server documentation
<https://docs.indiscale.com/caosdb-server/permissions.html#how-to-set-permissions>`_). Simply
add the following line to the file

.. code:: xml

   <Grant priority="true" role="curator"><Permission name="*"/></Grant>

This means that, by default, all users with the ``curator`` role are **granted**
all entity permissions (including insert, update, and delete as specified in the
beginning) **with priority**. This ensures, that no normal user is allowed to
overrule these permissions (since it is granted with priority), but it can still
be denied for the core data model entities by a **deny** rule with priority. See
the server documentation on `permission
calculation <https://docs.indiscale.com/caosdb-server/permissions.html#permission-calculation>`_
for more information on which permission rules can or can't be overruled.

Your complete ``global_entities_permissions.xml`` might then look like

.. code:: xml

   <globalPermissions>
       <Grant priority="false" role="?OWNER?"><Permission name="*"/></Grant>
       <Grant priority="false" role="?OTHER?"><Permission name="RETRIEVE:*"/></Grant>
       <Grant priority="false" role="?OTHER?"><Permission name="USE:*"/></Grant>
       <Grant priority="false" role="anonymous"><Permission name="RETRIEVE:*"/></Grant>
       <Grant priority="true" role="curator"><Permission name="*"/></Grant>
       <Deny priority="false" role="?OTHER?"><Permission name="UPDATE:*"/></Deny>
       <Deny priority="false" role="?OTHER?"><Permission name="DELETE"/></Deny>
       <Deny priority="true" role="?OTHER?"><Permission name="EDIT:ACL"/></Deny>
   </globalPermissions>

.. note::

   Note that you have to restart your LinkAhead server after modifying the
   ``global_entities_permissions.xml``.

The code
--------

After having applied all of the above prerequisites and restarting your LinkAhead
server, execute the following code.

:download:`Download full code<curator_permissions.py>`

.. literalinclude:: curator_permissions.py
