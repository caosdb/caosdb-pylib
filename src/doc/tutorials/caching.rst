
Caching
=======

.. note::

  Caching is great, because it can speed up things considerably. But it can also create dangerous
  pitfalls if the cache is not cleared when needed and you work with outdated data. Thus, please use
  the cache with care and make sure to clear it when needed.

Python provides great tools for caching. For example, you could define a ``cached_get_by_name``
function, easily created from ``get_entity_by_name`` using Python's ``lru_cache``:

.. code:: python

   @lru_cache(maxsize=1000)
   def cached_get_by_name(name):
       return db.get_entity_by_name(name)
  
   exp = cached_get_by_name('Experiment')
   # reset the cache with
   cached_get_by_name.cache_clear()

For convenience, PyLinkAhead provides the ``linkahead.cached`` module that defines the functions
``cached_query`` and ``cached_get_entity_by``, they use a shared cache. Let's have a look:

.. code:: python

   from linkahead.cached import cached_query, cached_get_entity_by, cache_clear, cache_info, cache_initialize
   rt1 = cached_get_entity_by(name='RT1')
   qresult = cached_query('FIND Experiment WITH parameter=1')
   # you can inspect the cache
   print(cache_info())
   # this will not cause a server request since it is cached
   rt1 = cached_get_entity_by(name='RT1')
   # you can clear the cache with
   cache_clear()
   # If you want to have a cache with a custom size, you can initialize it (again). Old cached
   # data is lost.
   cache_initialize(maxsize=10)


If you want to manually add entities to the cache, you can do it yourself. This is useful when you
have entities on hand from previous queries that you want to add.

.. code:: python

   from linkahead.cached import cache_fill, AccessType
   # Here, items must be a dict with Entity IDs as keys and the Entities as values.
   cache_fill(items, AccessType.EID, unique=True)
   # If you now use IDs that were in items, they are taken from the cache.
   e1 = cached_get_entity_by(eid=10001)

When filling the cache with Entity objects for ``cached_get_entity_by``, you need to set
``unique=True``, whereas the cache for ``cached_query`` should be filled with Container object and
``unique=False``.


