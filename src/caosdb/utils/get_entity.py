
from linkahead.utils.get_entity import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.utils.get_entity`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
