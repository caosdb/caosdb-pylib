
from linkahead.connection.authentication.input import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.connection.authentication.input`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
