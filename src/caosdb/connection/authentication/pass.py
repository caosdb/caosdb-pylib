
from linkahead.connection.authentication.pass import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.connection.authentication.pass`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
