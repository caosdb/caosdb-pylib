Administration
==============

The Python script ``linkahead_admin.py`` should be used for administrative tasks.
Call ``linkahead_admin.py --help`` to see how to use it.

The most common task is to create a new user (in the LinkAhead realm) and set a
password for the user (note that a user typically needs to be activated):

.. code:: console

   $ linkahead_admin.py create_user anna
   $ linkahead_admin.py set_user_password anna
   $ linkahead_admin.py add_user_roles anna administration
   $ linkahead_admin.py activate_user anna

