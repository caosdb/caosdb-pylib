
from linkahead.utils.create_revision import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.utils.create_revision`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
