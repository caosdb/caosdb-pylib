
from linkahead.utils.plantuml import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.utils.plantuml`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
