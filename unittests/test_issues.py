# This file is a part of the LinkAhead Project.
#
# Copyright (c) 2022 IndiScale GmbH
# Copyright (c) 2022 Daniel Hornung (d.hornung@indiscale.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Test known issues to prevent regressions.
"""

import os

import lxml
import linkahead as db

from datetime import date, datetime
from pytest import raises
from linkahead.common.utils import xml2str


def test_issue_100():
    """_parse_value() fails for some list-valued content
    """

    # Parse from (invalid) XML file
    filename = os.path.join(os.path.dirname(__file__), "data", "list_in_value.xml")
    xml_el = lxml.etree.parse(filename).getroot()
    with raises(TypeError) as exc_info:
        db.common.models._parse_single_xml_element(xml_el)
    assert "Invalid datatype: List valued properties" in str(exc_info.value)


def test_issue_156():
    """Does parse_value make a mistake with entities?

    https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/156
    """
    project = db.Record(name="foo")
    project.add_parent(name="RTName")
    # <Record name="foo">
    #   <Parent name="RTName"/>
    # </Record>
    experiment = db.Record()
    experiment.add_property(name="RTName", value=project)
    # <Record>
    #   <Property name="RTName" importance="FIX" flag="inheritance:FIX">foo</Property>
    # </Record>
    value = experiment.get_property("RTName").value
    # <Record name="foo">
    #   <Parent name="RTName"/>
    # </Record>
    parents = value.get_parents()
    # <ParentList>
    #   <Parent name="RTName"/>
    # </ParentList>
    assert value is project
    assert parents[0].name == "RTName"


def test_issue_128():
    """Test assigning datetime.date(time) values to DATETIME
    properties:
    https://gitlab.com/linkahead/linkahead-pylib/-/issues/128.

    """
    # Test assignement correct assignment for both datatype=DATETIME
    # and datatype=LIST<DATETIME>, just to be sure.
    prop = db.Property(name="TestDatetime", datatype=db.DATETIME)
    prop_list = db.Property(name="TestListDatetime", datatype=db.LIST(db.DATETIME))

    today = date.today()
    now = datetime.now()

    prop.value = today
    assert prop.value == today
    prop.value = now
    assert prop.value == now

    prop_list.value = [today, today]
    assert prop_list.value == [today, today]
    prop_list.value = [now, now]
    assert prop_list.value == [now, now]


def test_issue_73():
    """
    Test to_xml infinite recursion handling with cross- and self-references.
    https://gitlab.com/linkahead/linkahead-pylib/-/issues/73
    """
    # Cross-reference in the property values
    rt = db.RecordType(name="RT")
    recA = db.Record().add_parent(rt)
    recB = db.Record().add_parent(rt)
    recA.add_property(name="RT", value=recB)
    recB.add_property(name="RT", value=recA)
    xml_str = xml2str(recB.to_xml())
    assert "<Parent name=\"RT" in xml_str
    assert "<Property name=\"RT" in xml_str
    assert "Recursive reference" in xml_str
    assert len(xml_str) < 500

    # Cross-reference in the properties themselves
    prop1 = db.Property(name="Prop1")
    prop2 = db.Property(name="Prop2")
    prop1.add_property(prop2)
    prop2.add_property(prop1)
    xml_str = xml2str(prop2.to_xml())
    assert "<Property name=\"Prop1" in xml_str
    assert "<Property name=\"Prop2" in xml_str
    assert "Recursive reference" in xml_str
    assert len(xml_str) < 500

    # Self-reference in the datatype
    prop = db.Property()
    prop.datatype = prop
    xml_str = xml2str(prop.to_xml())
    assert "datatype=" in xml_str
    assert "Recursive reference" in xml_str
    assert len(xml_str) < 500
