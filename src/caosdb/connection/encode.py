
from linkahead.connection.encode import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.connection.encode`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
